def chess_board(pos_x, pos_y, board):
     return pos_x >= 0 and pos_y >= 0 and pos_x < blocks and pos_y < blocks and board[pos_x][pos_y] == -1

# Function to print Chessboard matrix
def matrix(blocks, board):
    for i in range(blocks):
        for j in range(blocks):
            print(board[i][j], end=' ')
        print()
        
def position_of_board(blocks):
    board = [[-1 for i in range(blocks)]for i in range(blocks)]
    
    # move_x and move_y define next move of Knight
    move_x = [2, 1, -1, -2, -2, -1, 1, 2]
    move_y = [1, 2, 2, 1, -1, -2, -2, -1]
    
    #Knight is initially at the first block
     board[0][0] = 0
    pos = 1

    if not knight_tour(blocks, board, 0, 0, move_x, move_y, pos):
        return "Solution does not exist"
    matrix(blocks, board)

def knight_tour(blocks, board, curr_x, curr_y, move_x, move_y, pos):
    if pos == blocks**2 :
        return True
    for i in range(8):
        new_x = curr_x + move_x[i]
        new_y = curr_y + move_y[i]

         
        if chess_board(new_x, new_y, board):
            board[new_x][new_y] = pos
            if knight_tour(blocks, board, new_x, new_y, move_x, move_y, pos+1):
                return True
            board[new_x][new_y] = -1
            
    return False
blocks = 8
position_of_board(blocks)


        
